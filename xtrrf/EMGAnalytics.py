import pandas as pd
import numpy as np
from scipy import signal as sg
#from scipy.signal import butter, sosfiltfilt, iirnotch, filtfilt
import attr

import matplotlib.pyplot as plt

@attr.s
class EMGAnalytics(object):
    ###################################################################################################################
    # EMG data analysis functions
    #   Author: Stas Steinberg (X-trodes LTD)
    #   Data: 30/4/2020
    ###################################################################################################################


    #def detect_onset(self, emg_data_df, lowpass=True, adaptive_threshold_sensitivity=3, smoothing_window=0.1, onset_duration=0.1):

        #smoothing_window = int(round(emg_data_df.fsample*smoothing_window)) #convert to samples
        #onset_duration = int(round(emg_data_df.fsample*onset_duration)) #convert to samples

        # calculate envelope
        #xa = pd.DataFrame()
        #for c in range(emg_data_df.num_of_channels):
        #    xa['Ch %d' % c] = pd.Series(sg.hilbert(emg_data_df.iloc[:, c]))
        #xa = xa.abs()
        #data['Envelope'] = xa

        # adaptive threshold using moving average
        #xa = xa.rolling(window=smoothing_window)




    def filter_data(self, data, filters):
        # filters is a dictionary of filters and parameters
        # possible filters are: 'notch', 'butter', 'demean'
        # order of application will be: demean, butter, notch
        # demean params: None
        # butter params: {'order': 5, 'lowcutoff': 20, 'highcutoff':500, 'samplerate': 4000} in Hz
        # notch params: {'f0': 50, 'Q': 30.0, 'samplerate': 4000}

        if filters is not None and data is not None:
            filt_data = np.copy(data)
            for fp in filters:
                print('Applying %s...\n' %fp)
                p = filters[fp]
                if fp == 'demean':
                    filt_data = sg.detrend(filt_data, axis=0, type='constant')
                if fp == 'butter':
                    filt_data = self.butter_bandpass_filter(filt_data, lowcut=p['lowcutoff'],
                                                       highcut=p['highcutoff'],
                                                       order=p['order'],
                                                       fs=p['samplerate'])
                if fp == 'notch':
                    filt_data = self.notch_filter(filt_data, fs=p['samplerate'],
                                             f0=p['f0'], Q=p['Q'])
            return filt_data
        else:
            return None

    def butter_bandpass_filter(self, data, lowcut, highcut, fs, order=5):
        sos = self.butter_bandpass(lowcut, highcut, fs, order=order)
        y = sg.sosfiltfilt(sos, data, axis=0)
        return y

    @staticmethod
    def butter_bandpass(lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        sos = sg.butter(order, [low, high], analog=False, btype='band', output='sos')
        return sos

    @staticmethod
    def notch_filter(data, fs, f0=50.0, Q=30.0):
        b, a = sg.iirnotch(f0, Q, fs)
        y = sg.filtfilt(b, a, data, axis=0)
        return y