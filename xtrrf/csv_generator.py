from tkinter import Tk
from tkinter.filedialog import askopenfilename
from xtrrf.XTRRF10Data import *

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = askopenfilename(title='Select NLE file', filetypes=(('NLE files', '*.NLE'), ('all files', '*.*')))
file = Path(filename)
workpath = file.parent
data = DataObj(workpath=workpath)
data.load_events()