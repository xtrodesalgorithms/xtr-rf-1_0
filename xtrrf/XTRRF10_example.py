from xtrrf.XTRRF10Data import *
from xtrrf.EMGAnalytics import *
import pandas as pd
from tkinter import filedialog
from tkinter import *

#proc = EMGAnalytics()

## Loading data

root = Tk()
root.selected_folder = filedialog.askdirectory()

some_data = DataObj(workpath=root.selected_folder)

# reading data into RAM
#some_data.read_data()

## Saving data from RAM
#some_data.save_as(save_as='mat') # this will save the data and the events as MATLAB files in the workpath
# some_data.save_as(save_as='csv') # this will save the data as a CSV in the workpath (eventlog.csv already exists)
# some_data.save_as(save_as='hdf') # this will save the data and the events as an H5 file in the workpath

# streaming data to HDF5 directly
some_data.stream_to_hdf5()

## Using filters
#filters = {'demean':None,
#       'butter':{'order': 5, 'lowcutoff': 20, 'highcutoff':500, 'samplerate': 4000},
#       'notch':{'f0': 50, 'Q': 30.0, 'samplerate': 4000}}

# for the purpose of generalization of the Analytics class, the functions do not return dataframes
#filt_data_df = pd.DataFrame(proc.filter_data(some_data._data_df.iloc[:,:some_data.num_channels], filters))
# add the column names and Time vector from raw data
#filt_data_df['Time'] = some_data._data_df['Time'].values
#filt_data_df.columns = some_data._data_df.columns

## plot using MatPlotLib without subplots
#some_data.plot(df_to_plot=filt_data_df, channels=[1,2], scale_multiplier=1000000, overlaying=True, offset=0)

