import sys
import subprocess
import json
import numpy as np
import attr
import scipy.io as sio
from pathlib import Path
import re
from tqdm import tqdm
import pandas as pd
# import plotly.graph_objects as go
# import plotly.offline as py
# import holoviews as hv
# from holoviews import opts
# from holoviews.operation.datashader import datashade, dynspread
# import panel as pn
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.gridspec as gridspec
import xtrrf
#import tables
from time import sleep
import gc
matplotlib.use('TkAgg')
# hv.extension('bokeh')


@attr.s
class DataObj(object):
    ###################################################################################################################
    # Data container object for XTR-RF 1.0 files.
    #   Author: Stas Steinberg (X-trodes LTD)
    #   Data: 30/4/2020
    ###################################################################################################################

    workpath = attr.ib(converter=Path)
    num_ADC_bits = attr.ib(default=15)
    num_channels = attr.ib(default=8)
    fsample = attr.ib(default=4000)

    _data_df = attr.ib(default=None, kw_only=True)
    _events_df = attr.ib(default=None, kw_only=True)
    _metadata = attr.ib(default=None)

    @staticmethod
    def _conv_binary_data_file_to_volts(file_path, number_of_channels, voltage_resolution, num_ADC_bits):
        try:
            data = np.fromfile(file_path, dtype=np.uint16)
        except:
            raise IOError('Problem reading file: {}'.format(file_path))

        data = np.reshape(data, (number_of_channels, -1), order='F')
        data = np.multiply(voltage_resolution,
                           (data - np.float_power(2, num_ADC_bits - 1)))

        return data

    def load_events(self, columns=('event_number', 'event_timestamp', 'event_time', 'event_time_source', 'event_type', 'event_details')):
        # csv_file_path = next(Path(self.workpath).glob('EVENTLOG.CSV'))
        csv_file_path = self.workpath.joinpath('EVENTLOG.CSV')
        if not csv_file_path.is_file():
            print('Could not find CSV file in path %s\n' % csv_file_path)
        #    return 0
            event_reader_path = [x for x in Path(xtrrf.__file__).parent.glob('*Event_File_Reader*')][0]
            if not event_reader_path.is_file():
                print('Cannot find NLE reader at path %s' % event_reader_path)
                sys.exit()
            event_file_path = self.workpath.joinpath('EVENTLOG.NLE')
            if not event_file_path.is_file():
                print('Cannot find NLE file at path %s' % event_file_path)
                sys.exit()
            cmd_string = '"' + str(event_reader_path) + '" "' + str(event_file_path) + '" ' + ' "' + str(csv_file_path) + '" tab'
            subprocess.call(cmd_string)
            sleep(1)
            #csv_file_path = next(Path(self.workpath).glob('EVENTLOG.CSV'))
            if not csv_file_path.is_file():
                print('CSV log was not created from NLE file! Try running %s manually.' % event_reader_path)
                return 0
        try:
            events_df = pd.read_csv(csv_file_path, low_memory=False, skiprows=2, header=None, sep='\t')
        except:
            print('Warning: CSV file delimiter issue. Are you sure that the CSV delimited is tab? Trying comma delimiter...\n')
            try:
                events_df = pd.read_csv(csv_file_path, low_memory=False, skiprows=2, header=None, sep=',')
            except:
                print('Something is wrong with the CSV. Try opening it and fix the problem manually.')
                return 0

        #events_df.drop(events_df.columns[0], axis=1, inplace=True)
        if columns is not None:
            events_df.columns = columns


        # Process session details
        metadata = dict()
        with open(csv_file_path,'r') as eventlogfile:
            session_details = eventlogfile.readline()

        firmware_version_re = re.compile('(?<=Firmware Version = )(.*)(?=\; Se)')
        firmware_version = re.findall(firmware_version_re, session_details)
        metadata['Firmware_version'] = firmware_version[0]

        serial_number_re = re.compile('(?<=Serial Number = )(.*)(?=\; T)')
        serial_number = re.findall(serial_number_re, session_details)
        metadata['Serial number'] = serial_number[0]

        session_start_time_re = re.compile('(?<=Time = )(.*)(?=\; D)')
        session_start_time = re.findall(session_start_time_re, session_details)
        metadata['Session start time'] = session_start_time[0]

        session_start_date_re = re.compile('(?<=Date = )(.*)(?=\; Samp)')
        session_start_date = re.findall(session_start_date_re, session_details)
        metadata['Session start date'] = session_start_date[0]

        adc_period_re = re.compile('(?<=Period = )(.*)(?=us)')
        adc_period = re.findall(adc_period_re, session_details)
        metadata['ADC period'] = np.float(adc_period[0])

        adc_resolution_re = re.compile('(?<=ADC Resolution = )(.*)(?=uV)')
        voltage_resolution = re.findall(adc_resolution_re, session_details)
        metadata['Voltage resolution'] = np.float(voltage_resolution[0]) * 10 ** (-6)

        neuro_file_size_re = re.compile('(?<=Neural Data File Size = )(.*)(?=MBytes)')
        neuro_file_size = re.findall(neuro_file_size_re, session_details)
        metadata['Neuro file size'] = np.int(np.floor(np.float(neuro_file_size[0])) * 1024 ** 2)

        max_num_of_files_re = re.compile('(?<=Maximum number of files = )(.*)(?=\; E)')
        max_num_of_files = re.findall(max_num_of_files_re, session_details)
        metadata['Max number of files'] = np.int(max_num_of_files[0])

        self._metadata = metadata

        # take care of midnight when timestamps are reset to 0
        # find where the location of midnight based on two criteria:
        # 1. find where diff(time from midnight) < 0 - this is a suspect for post-midnight timestamp, let's call
        #   this moment in time n_s.
        # 2. check whether event_numbers satisfy: n_s - 1 < n_s < n_s + 1 - just in case the event n_s was registered
        #   after event n_s - 1, but occurred earlier in "lab time".
        # 3. event_time_source must be the logger, otherwise it's some pathological event
        midnight_idx_list = list()
        suspect_midnight_idx = np.where(events_df['event_time'].diff() < -23*60*60*1000)[0]
        for idx in suspect_midnight_idx:
            if idx < (events_df.shape[0] - 1):
                if (events_df['event_number'][idx-1] < events_df['event_number'][idx]) and \
                        (events_df['event_number'][idx] < events_df['event_number'][idx + 1]):
                    if events_df['event_time_source'][idx]=='Logger':
                        midnight_idx_list.append(idx)

        # most likely midnight_idx_list will be of lenght 1, but let's assume that we recorded for many days
        # for each time midnight was crossed, we will accumulate the previous milliseconds up to that point in time

        for idx in midnight_idx_list:
            events_df.loc[idx:, 'event_time'] = events_df.loc[idx:, 'event_time'] + events_df.loc[idx-1, 'event_time']

        # start_of_recoridng = pd.Index(events_df.iloc[:, 4].str.contains('Started recording'))
        # t=0 is the time when the first file was created
        start_of_recoridng = pd.Index(events_df['event_type'].str.contains('File started'))
        event_times_start_of_recording = np.array(events_df['event_time'][start_of_recoridng])
        events_df['event_time'] = events_df['event_time'] - event_times_start_of_recording[0]
        events_df['event_time'] = events_df['event_time'].div(1000)


        self._events_df = events_df


    def read_data(self):
        # get session details
        self.load_events()

        file_list = [file for file in self.workpath.glob('*.DT8')]
        data_chunks = list()
        file_started = self._events_df['event_type'].str.contains('File started')
        event_times_file_started = np.array(self._events_df['event_time'][file_started])
        # = np.float(self._events_df['event_time'].tail(1))

        # check that the num of files suit the num of file creation events
        if event_times_file_started.shape[0] < len(file_list):
            print('\n --- WARNING: There are more files than recorded file events ---')

        for count, file in enumerate(tqdm(file_list)):

            # break the loop if there are more files than events!
            if count >= event_times_file_started.shape[0]:
                break

            data_chunk = self._conv_binary_data_file_to_volts(file,
                                                              self.num_channels,
                                                              self._metadata['Voltage resolution'],
                                                              self.num_ADC_bits)

            time_tag_of_file_creation = event_times_file_started[count]

            start_time = time_tag_of_file_creation
            end_time = start_time + (data_chunk.shape[1]-1) / self.fsample

            time_vec = np.linspace(start_time, end_time, data_chunk.shape[1])

            # Find end time for the current chunk
            # We are looking for the first 16 samples which values do not change
            # this happens when recording was stopped before a file was filled
            consecutives = zero_runs(np.diff(data_chunk[0, :], axis=0))
            temp = consecutives[((consecutives[:, 1] - consecutives[:, 0]) > 16)]
            if len(temp) != 0:
                end_idx = int(temp[0, 0])
                # end_idx here is the first index
                time_vec = time_vec[0:end_idx]
                data_chunk = data_chunk[:, 0:end_idx]

            data_chunk_df = pd.DataFrame(data_chunk.T, columns=['Ch%d' % (c) for c in range(self.num_channels)])
            data_chunk_df['Time'] = pd.Series(time_vec)
            data_chunks.append(data_chunk_df)

        data_chunks_df = pd.concat(data_chunks)
        data_chunks_df['Time'] = pd.Series(data_chunks_df['Time']) - data_chunks_df['Time'].iloc[0]
        self._data_df = data_chunks_df

    def plot(self, df_to_plot=None, plot_events=True, offset=None, time_range=None, channels=None, ds_factor=None,
             scale_multiplier=None, overlaying=False):
        # if df_to_plot is None it will always plot raw_data
        # flag_plot_events = plot_events
        if df_to_plot is None:
            data = self._data_df
        else:
            if not isinstance(df_to_plot, pd.DataFrame):
                print('Data must be passed as a DataFrame!')
                return 0
            else:
                data = df_to_plot
        time = self._data_df['Time']

        if channels is None:
            channels = [x for x in range(self.num_channels)]

        if time_range is None:
            data = data.iloc[:, channels]
        else:
            data = data.iloc[time_range[0] * self.fsample: time_range[1] * self.fsample, :].iloc[:, :-1]
            data = data.iloc[:, channels]
            time = time.iloc[time_range[0] * self.fsample: time_range[1] * self.fsample]

        if ds_factor is not None:
            print('\nDecimating by %d...' % ds_factor)
            time = time.iloc[::ds_factor]
            data = data.iloc[::ds_factor, :]
        # if offset is None:
        # calculate offset
        #    offset = 0.5 * data.iloc[:, channels].max().max()
        if scale_multiplier is not None:
            data = data * scale_multiplier

        event_lines = list()
        if plot_events == True:
            start_of_recording = self._events_df['event_details'].str.contains('Started recording')
            event_times_start_of_recording = np.array(self._events_df['event_time'][start_of_recording])
            if time_range is not None:
                event_times_start_of_recording = event_times_start_of_recording[
                    time_range[0] <= event_times_start_of_recording]

            end_of_recording = self._events_df['event_details'].str.contains('Stopped recording')
            event_times_end_of_recording = np.array(self._events_df['event_time'][end_of_recording])
            if time_range is not None:
                event_times_end_of_recording = event_times_end_of_recording[
                    time_range[1] > event_times_end_of_recording]

            file_started = self._events_df['event_type'].str.contains('File started')
            event_times_file_started = np.array(self._events_df['event_time'][file_started])
            if time_range is not None:
                event_times_file_started = event_times_file_started[(event_times_file_started >= time_range[0]) &
                                                                    (event_times_file_started < time_range[1])]

            dig_in = self._events_df['event_type'].str.contains('Digital in')
            event_times_dig_in = np.array(self._events_df['event_time'][dig_in])
            if time_range is not None:
                event_times_dig_in = event_times_dig_in[(event_times_dig_in >= time_range[0]) &
                                                        (event_times_dig_in < time_range[1])]

            free_text = self._events_df['event_type'].str.contains('Free text')
            event_times_free_text = np.array(self._events_df['event_time'][free_text])
            if time_range is not None:
                event_times_free_text = event_times_free_text[(event_times_free_text >= time_range[0]) &
                                                              (event_times_free_text < time_range[1])]

            if len(event_times_start_of_recording) > 0:
                for ev in event_times_start_of_recording:
                    event_lines.append({'x': ev,
                                        'ymin': data.min().min(),
                                        'ymax': data.max().max(),
                                        'color': 'green'})

            if len(event_times_dig_in) > 0:
                for ev in event_times_dig_in:
                    event_lines.append({'x': ev,
                                        'ymin': data.min().min(),
                                        'ymax': data.max().max(),
                                        'color': 'red'})

            if len(event_times_end_of_recording) > 0:
                for ev in event_times_end_of_recording:
                    event_lines.append({'x': ev,
                                        'ymin': data.min().min(),
                                        'ymax': data.max().max(),
                                        'color': 'orange'})

            if len(event_times_file_started) > 0:
                for ev in event_times_file_started:
                    event_lines.append({'x': ev,
                                        'ymin': data.min().min(),
                                        'ymax': data.max().max(),
                                        'color': 'purple'})

            if len(event_times_free_text) > 0:
                for ev in event_times_free_text:
                    event_lines.append({'x': ev,
                                        'ymin': data.min().min(),
                                        'ymax': data.max().max(),
                                        'color': 'blue'})

        if overlaying == False:
            fig = plt.figure(constrained_layout=True)
            gs = gridspec.GridSpec(nrows=data.shape[1], ncols=1, figure=fig)

            ax = list()
            for ii, ch in enumerate(channels):
                if ii == 0:
                    ax.append(fig.add_subplot(gs[ii, :],
                                              ylabel='Ch%d [mV]' % ch))

                elif ii == len(channels) - 1:
                    ax.append(fig.add_subplot(gs[ii, :],
                                              sharex=ax[0],
                                              sharey=ax[0],
                                              ylabel='Ch%d [mV]' % ch,
                                              xlabel='Time [s]'))
                else:
                    ax.append(fig.add_subplot(gs[ii, :],
                                              sharex=ax[0],
                                              sharey=ax[0],
                                              ylabel='Ch%d [mV]' % ch))

            for ii, ch in enumerate(channels):
                ax[ii].plot(time, data.iloc[:, ii])
                ax[ii].grid()
                for line in event_lines:
                    ax[ii].axvline(x=line['x'], ymin=line['ymin'], ymax=line['ymax'],
                                   color=line['color'])

        else:
            fig = plt.figure()
            if offset is None:
                # calculate offset
                offset = 0.5 * data.max().max()

            ax = fig.add_axes((0, 0, 1, 1))
            for ii, ch in enumerate(channels):
                ax.plot(time, data.iloc[:, ii] + ii * offset, label='Ch%d' % ch)
            for line in event_lines:
                ax.axvline(x=line['x'], ymin=line['ymin'], ymax=line['ymax'],
                           color=line['color'])
            ax.grid()
            ax.legend(loc='upper right')

        plt.show()

    def save_as(self, folder_to_save_to=None, save_as=None, save_json=True):
        data = self._data_df
        events = self._events_df
        metadata = self._metadata
        if folder_to_save_to is None:
            save_to = self.workpath
        else:
            save_to = Path(folder_to_save_to)

        print('Saving %s to %s\n' % (save_as, str(save_to)))

        if save_as == 'mat':
            a_dict = {col_name: data[col_name].values for col_name in data.columns.values}
            try:
                sio.savemat(save_to.joinpath('data.mat'), {'data': a_dict})
            except:
                print('Could not save MAT file. Too much data?\n')
            a_dict = {col_name: events[col_name].values for col_name in events.columns.values}
            sio.savemat(save_to.joinpath('events.mat'), {'events': a_dict})
        elif save_as == 'csv':
            data.to_csv(save_to.joinpath('data.csv'))
            events.to_csv(save_to.joinpath('events.csv'))
        elif save_as == 'hdf':
            hdf_store = pd.HDFStore(path=save_to.joinpath('data_and_events.h5'), mode='w', complevel=9, complib='blosc:lz4')
            hdf_store.append('data', data)
            hdf_store.append('events', events, format='table')
            hdf_store.close()
        elif save_as == None:
            print('WARNING: no file format chosen. Saving only metadata in JSON.\n')
        else:
            print('ERROR: %s is not a valid saving format, choose between mat, hdf5 and csv.\n')

        if save_json == True:
            with open(save_to.joinpath('metadata.json'), 'w') as fp:
                json.dump(metadata, fp, sort_keys=True, indent=4)

        print('Finished!\n')


    def stream_to_hdf5(self, folder_to_save_to=None):
        if folder_to_save_to is None:
            save_to = self.workpath
        else:
            save_to = Path(folder_to_save_to)

        hdf5_file_name = Path(save_to)
        hdf5_file_name = hdf5_file_name.joinpath('data.h5')
        hdf5_file = pd.HDFStore(path=hdf5_file_name, mode='w', complevel=9, complib='blosc:lz4')

        # get session details
        self.load_events()
        file_started = self._events_df['event_type'].str.contains('File started')
        event_times_file_started = np.array(self._events_df['event_time'][file_started])
        file_list = [file for file in self.workpath.glob('*.DT8')]
        if event_times_file_started.shape[0] < len(file_list):
            print('\n --- WARNING: There are more files than recorded file events ---')


        for count, file in enumerate(tqdm(file_list)):
            # break the loop if there are more files than events!
            if count >= event_times_file_started.shape[0]:
                break
            data_chunk = self._conv_binary_data_file_to_volts(file,
                                                              self.num_channels,
                                                              self._metadata['Voltage resolution'],
                                                              self.num_ADC_bits)

            time_tag_of_file_creation = event_times_file_started[count]
            start_time = time_tag_of_file_creation
            end_time = start_time + (data_chunk.shape[1] - 1) / self.fsample

            time_vec = np.linspace(start_time, end_time, data_chunk.shape[1])

            # Find end time for the current chunk
            # We are looking for the first 16 samples which values do not change
            # this happens when recording was stopped before a file was filled
            consecutives = zero_runs(np.diff(data_chunk[0, :], axis=0))
            temp = consecutives[((consecutives[:, 1] - consecutives[:, 0]) > 16)]
            if len(temp) != 0:
                end_idx = int(temp[0, 0])
                # end_idx here is the first index
                time_vec = time_vec[0:end_idx]
                data_chunk = data_chunk[:, 0:end_idx]

            data_chunk_df = pd.DataFrame(data_chunk.T, columns=['Ch%d' % (c) for c in range(self.num_channels)])
            data_chunk_df['Time'] = pd.Series(time_vec)
            hdf5_file.append('data', data_chunk_df, format='t')

            if np.mod(count, 5) == 0:
                # every fifth iteration flush data into hard disk
                hdf5_file.flush(fsync=True)
                gc.collect() # enforce garbage collection

        hdf5_file.append('events', self._events_df, format='table')
        hdf5_file.close()
        self.save_as()

def zero_runs(a):
    # Create an array that is 1 where a is 0, and pad each end with an extra 0.
    iszero = np.concatenate(([0], np.equal(a, 0).view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    # Runs start and end where absdiff is 1.
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)
    return ranges

