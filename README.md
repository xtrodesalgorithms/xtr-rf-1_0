
# XTR-RF 1.0 Parsing and Analysis tools

# Installation
### Prerequisites:

1. Python 3.6, 3.7 or 3.8 (stable), 64-bit only.
2. Python virtual environment with pip.

### Installation steps:

1. Open a new python project.
2. Create a python virtual environment (using venv, pipenv, anaconda and so on...) and activate it. We recommend using venv.
3. Run the following:

		pip install gitpython
		pip install wheel
		pip install git+https://bitbucket.org/xtrodesalgorithms/xtr-rf-1_0.git

*Note: you do not need to clone this repo, pip will download and install everything automatically.*

*Note: if "pip install gitpython" doesn't work, try "pip install git".*

# Updating
From the relevant virtual environment, run the following:

		pip uninstall xtrrf
		pip install git+https://bitbucket.org/xtrodesalgorithms/xtr-rf-1_0.git

# Usage
## Example
The following example (see XTRRF10_example.py) uses the data container class (XTRRF10Data) and a simple analytics class (EMGAnalytics) to load data, save it in .MAT and .CSV formats, filter it and plot it using MatPlotLib. More details on the classes is below.

*XTRRF10_example.py*:

	from xtrrf.XTRRF10Data import *
	from xtrrf.EMGAnalytics import *
	import pandas as pd
	from tkinter import filedialog
	from tkinter import *

	proc = EMGAnalytics()

	## Loading data

	root = Tk()
	root.selected_folder =  filedialog.askdirectory()

	some_data = DataObj(workpath = root.selected_folder)
	some_data.read_data()

	## Saving data
	#some_data.save_as(save_as='mat') # this will save the data and the events as MATLAB files in the workpath
	#some_data.save_as(save_as='csv') # this will save the data as a CSV in the workpath (eventlog.csv already exists)
	some_data.save_as(save_as='hdf') # this will save the data and the events as an H5 file in the workpath

	## Using filters
	filters = {'demean':None,
		   'butter':{'order': 5, 'lowcutoff': 20, 'highcutoff':500, 'samplerate': 4000},
		   'notch':{'f0': 50, 'Q': 30.0, 'samplerate': 4000}}

	# for the purpose of generalization of the Analytics class, the functions do not return dataframes
	filt_data_df = pd.DataFrame(proc.filter_data(some_data._data_df.iloc[:,:some_data.num_channels], filters))
	# add the column names and Time vector from raw data
	filt_data_df['Time'] = some_data._data_df['Time'].values
	filt_data_df.columns = some_data._data_df.columns

	## plot using MatPlotLib without subplots
	some_data.plot(df_to_plot=filt_data_df, channels=[1,2], scale_multiplier=1000000, overlaying=True, offset=0)

## XTR-RF Data Class

**class DataObj(object)**:  
     
Data container object for XTR-RF 1.0 files. 

**Properties**: 

1. workpath -  absolute path to the directory where the binary files and eventlog are located. E.g. "C:/Users/Admin/Documents/Files"  (use forward slash) 

2. num_ADC_bits - the number of ADC bits used for conversion (15 by default) 

3. num_channels - the number of channels recorded (8 by default) 

4. fsample - the sampling rate (4000 by default) 


**Protected properties**:   

1. _data_df - a pandas DataFrame which holds the data ([samples X channels] in volts) and time vector (in seconds). 

2. _events_df - a pandas DataFrame which holds the event log data.  

3. _metadata - a dictionary which holds device parameters and various metadata (see in code). 

**Methods**: 

1. read_deuteron_data() - processes the binary files in workpath and stores data in the protected properties. 

2. load_deuteron_events(columns) - processes the eventlog.cls file. 

		columns - the names of the column headers of the self._event_df log table. 

2. _conv_binary_data_file_to_volts(file_path, number_of_channels, voltage_resolution, num_ADC_bits) - does the actual conversion of a binary file into a voltage matrix (works per file). 

3. plot_pyplot(df_to_plot=None, plot_events=True, offset=None, time_range=None, channels=None, ds_factor=None):
Plots the data in df_to_plot using PyPlot (can be very slow if many points are plotted). Inputs that are None by default are optional.        

		df_to_plot - a table of [[samples X channels], [Time]]. Last column is a time vector. 
		plot_events - a flag that triggers event plotting as colored vertical bars. 
		offset - a value (in volts) that separates between different channels in the plot. 
		time_range - a tuple of two values (min, max) that defines the time frame to plot. 
		channels - a tuple that defines which channels to plot. 
		ds_factor - a downsampling factor, to be used in case too many points are plotted and PyPlot crashes. 

5. plot_shader(...):    
Plots the data in df_to_plot using DataShader (fastest method for a lot of points). 
same as self.plot_pyplot(...), except that there is no offset parameter since subplots are used. 

6. plot(...):   
Plots the data in df_to_plot using matplotlib (faster than PyPlot, slower than DataShader). 
overlaying - if True, subplots are NOT used. False by default. Otherwise same as self.plot_pyplot(...) 

7. save_as(...): 
Saves data, events as mat file or csv file. Metadata is saved as json. 

		folder_to_save_to - an absolute path to an empty directory where the data files will be saved at. 
		save_as - either 'mat', 'csv' or 'hdf'.  Note: HDF will be saved with compression level 9 and compression library 'blosc:lz4'.

## XTR EMG Analytics class
**class EMGAnalytics(object)**:

EMG data analysis functions.

**Properties**:  
N/A  

**Protected properties**:  
N/A  


**Methods**:  

1. filter_data(...) - notch (50Hz, Q=30) & bandpass filters a matrix of data using  
                                                        a butterworth IIR filter of order 5.  
                                                        
	    data - [samples X channels] in volts.  
	    lowcut - low cutoff frequency (Hz).  
	    highcut - high cutoff frequency (Hz).  
	 
 2. butter_bandpass_filter(data, lowcut, highcut, fs, order=5) - helper method which calls the SOS zero-phase  
                                                                    filtering function.  
 
 3. butter_bandpass(lowcut, highcut, fs, order=5) - helper method that defines the bandpass filter.  

 4. notch_filter(data, fs, f0=50.0, Q=30.0) - helper method that defines the notch filter.  
