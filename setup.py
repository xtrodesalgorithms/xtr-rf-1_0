from setuptools import setup, find_packages


with open('requirements.txt') as fp:
    install_requires = fp.read()

    setup(
        name='xtrrf',
        version='1.1dev5',
        author='Stas Steinberg (X-trodes LTD)',
        author_email='stas@xtrodes.com',
        packages=['xtrrf'],
        include_package_data=True,
        license='GNU GPLv3',
        long_description=open('README.md').read(),
        url="https://bitbucket.org/xtrodesalgorithms/xtr-rf-1_0",
        install_requires=install_requires
    )